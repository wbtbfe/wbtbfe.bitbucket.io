$(document).click(function(event) { 
    var $target = $(event.target);
    if(!$target.parents(".search-element").length && $('.collapsed-content').is(":visible")) {
        $(".filter-bar .collapsed-content").hide();
        $(".filter-bar .collapsible").removeClass("active");
    }        
});
$(".filter-bar .collapsible").click(function () {
    $(".filter-bar .collapsible").not(this).removeClass('active')
    $(".filter-bar .collapsible").not(this).next(".collapsed-content").hide(50);
});