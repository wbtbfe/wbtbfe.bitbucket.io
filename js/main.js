/* 0. Common script */

// get the search term value and open the modal

$('#search-input').keyup(function () {
   console.log($(this).val());
   var value = $(this).val();
   if([...value].length === 3) {
       // alert("good");
        $('.advance-search-blk').removeClass('d-none');
        $('#advance-search').val(value);
        $('#advance-search').focus();
   }
});

$('#advance-search').keyup(function () {

    var isHidden = $('ul.highlight_list li').is(":visible");
    console.log(isHidden);
    if(!isHidden) {
        $('.no-result').removeClass('d-none');
    }
    if (isHidden || $('#advance-search').val() == '' ) {
        $('.no-result').addClass('d-none');
    }
})

$('.close-blk .link-btn').on('click', function () {
    $('#search-input').val('');
    $('.advance-search-blk').addClass('d-none');
});

$('.search-result-page .close-blk .link-btn').on('click', function () {
    $('body').removeClass('no-overflow');
});

// $('body').keypress(function(e){
//     if(e.which == 27){
//         if(!$('.advance-search-blk').hasClass('d-none'))
//         $('.advance-search-blk').addClass('d-none');
//     }
// });

// $('#more-link').on('click', function () {
//     $(this).addClass('d-none');
// })
/* 1. Testimonial Active*/
// $(document).ready(function(){
//     $('.owl-carousel').owlCarousel({
//         stagePadding: 50,
//         loop:true,
//         margin:10,
//         nav:true,
//         responsive:{
//             0:{
//                 items:1
//             },
//             600:{
//                 items:3
//             },
//             1000:{
//                 items:5
//             }
//         }
//     })
// });

/* 2. Select2 */
$('#city-select').select2({
    selectOnClose: true,
    dropdownPosition: 'auto',
    language: {
        noResults: function (params) {
            return "لا يوجد نتائج";
        }
    }
});

/* 3. HideSeek */
$(document).ready(function() {
    $('#advance-search').hideseek({
        highlight: true,
        headers: '.origin',
        nodata: 'No results found'

    });
    // $('#search-input').hideseek({
    //     highlight: true,
    //     headers: '.origin'
    // });
});
/* 4. Open Filter */
$('.filter-blk img').on('click', function () {
    $('.advance-search-blk').removeClass('d-none');
    $('body').addClass('no-overflow');

});


$('.filter-form .close').on('click', function () {
    $('.advance-search-blk').addClass('d-none');
});
$('.notification .close-notification').on('click', function () {
    $(this).parents('.notification').hide(0);
});

/* 5. Swiper Slider  */

var swiper1 = new Swiper('.first-slide', {
    loop: true,
    navigation: {
        nextEl: '.next-1',
        prevEl: '.prev-1',
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
    breakpoints: {
        // when window width is >= 640px
        1200: {
            slidesPerView: 4,
            spaceBetween: 32
        },
        900: {
            slidesPerView: 4,
            spaceBetween: 32
        },
        768: {
            slidesPerView: 1.5,
            spaceBetween: 8
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 1.5,
            spaceBetween: 8
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1.5,
            spaceBetween: 8
        },
        // when window width is >= 320px
        320: {
            slidesPerView: 1.2,
            spaceBetween: 8
        },
    },
    spaceBetween: 50,
});
var swiper2 = new Swiper('.second-slide', {
    loop: true,
    spaceBetween: 0,
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
    navigation: {
        nextEl: '.next-2',
        prevEl: '.prev-2',
    },
    breakpoints: {
        // when window width is >= 640px
        640: {
            slidesPerView: 4,
        },
        // when window width is >= 320px
        320: {
            slidesPerView: 2.2,
        },
    },

});
