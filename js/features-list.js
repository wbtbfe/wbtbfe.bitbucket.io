$(function () {
  $(window).scroll(function () {
    var hT = $(".card").first().offset().top,
      sT = $("#features-list-section").offset().top,
      wS = $(this).scrollTop(),
      wH = $(window).height(),
      lastCardTop = $(".card").last().offset().top,
      lastCardHight = $(".card").last().outerHeight(),
      isFixed = $(".register-btn-container").hasClass("fixed");

    if (window.outerWidth < 767) {
      if (wS > hT - sT && wS < lastCardTop + lastCardHight - wH) {
        if (!isFixed) {
          setTimeout(function(){
            $('#card-container').css("top", "+=78");
            $(".register-btn-container")
              .fadeOut(10)
              .addClass("fixed")
              .fadeIn(400);
          },0)
        }
      } else {
        if (isFixed) {
          setTimeout(function(){
            $('#card-container').css("top", "-=78");
            $(".register-btn-container")
              .fadeOut(10)
              .removeClass("fixed")
              .fadeIn(400);
          },0)
        }
      }
    }
  });
});
