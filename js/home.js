$(function() {
    
    if (window.outerWidth > 1200){
        $('.accordion-tabs .collapsible').not('.active').addClass('active');
        $('.accordion-tabs .collapsible').next(".collapsed-content").show();
    }

});