function switchLogic() {
    $('#monthly-tag , #annually-tag').removeClass('text-blue')
    if ($("#subscribe-monthly").is(':checked')) {
        $('.bundle-container.annually').hide();
        $('.bundle-container.monthly').show();
        $('#monthly-tag').addClass('text-blue');
    } else {
        $('.bundle-container.monthly').hide();
        $('.bundle-container.annually').show();
        $('#annually-tag').addClass('text-blue');
    }
}
$('.bundle-card').on('click', function () {
    $(this).siblings('.bundle-card').removeClass('active');
    $(this).addClass('active')
});
$('.switch').on('click', function (e) {
    switchLogic();
});
$('#switch-to-annual').on('click',function(){
    $("#subscribe-monthly").prop( "checked", false );
    switchLogic();
})
$(".collapsible").on('click',function () {
    if($(this).hasClass('active')){
        $(this).parents(".question").addClass("active");
    }else{
        $(this).parents(".question").removeClass("active");

    }

  });
  const swiper = new Swiper('.swiper', {
      // Default parameters
      initialSlide: 0,
      loop: true,
  slidesPerView: 1.2,
  spaceBetween: 10,
  centeredSlides: true,
  centeredSlidesBounds:true,
  centerInsufficientSlides:true,
  // Responsive breakpoints
  navigation: {
    nextEl: '.next-1',
    prevEl: '.prev-1',
},
pagination: {
    el: '.swiper-pagination',
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    hide: true,
},
  });
 