      // ============== start variables  ===============

      var showChar = screen.width>1200?550:250;
      var ellipsestext = "...";
      var moretext = "أقل";
      var lesstext = "المزيد";
      // ============== end variables ===============
      // ============== start events ===============

      $(".more").each(function () {
        var content = $(this).text();

        if (content.length > showChar) {
          var c = content.substr(0, showChar);
          var h = content.substr(showChar - 1, content.length - showChar);

          var html =
            c +
            '<span class="more-ellipses">' +
            ellipsestext +
            '</span>&nbsp;<span class="more-content"><span class="hide" style="display:none;">' +
            h +
            '</span>&nbsp;&nbsp;<a href="" class="more-link less">' +
            lesstext +
            "</a></span>";
          
          $(this).html(html);
        }
      });

      $(".more-link").click(function () {
        if ($(this).hasClass("less")) {
          $(this).removeClass("less");
          $(this).html(moretext);
        } else {
          $(this).addClass("less");
          $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
      });
      // ============== end events ===============

