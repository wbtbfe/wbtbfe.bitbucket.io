
$(function () {
  AOS.init();

    $(window).scroll(function () {
      var  sT = $("#features-list-section").offset().top,
        wS = $(this).scrollTop(),
        wH = $(window).height(),
        lastCardTop = $(".participated-doctors-Wrapper").last().offset().top,

        isFixed = $(".sticky-register-btn-container").hasClass("sticky-on");
  
      if (window.innerWidth < 767) {
        if (wS >  sT-100 && wS < lastCardTop  - wH) {
          if (!isFixed) {
            setTimeout(function(){
              $('#card-container').css("top", "+=78");
              $(".sticky-register-btn-container")
                .addClass("sticky-on")
                .fadeIn(400);
            },0)
          }
        } else {
          if (isFixed) {
            setTimeout(function(){
              $('#card-container').css("top", "-=78");
              $(".sticky-register-btn-container")
                .fadeOut(10)
                .removeClass("sticky-on")
            },0)
          }
        }
      }
    });
  });