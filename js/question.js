$(function() {
    $(".collapsible").click(function () {
      $(this).toggleClass("active");
      $(this).parents(".question").toggleClass("active");
      $(this).next().toggle(0);
    });
    $(".content").hide();
});