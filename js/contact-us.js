        // ============== start variables  ===============
        const text = {
            doctor: 'doctor',
            hospital: 'hospital',
        }
        const content = {
        };
        // ============== end variables ===============
        // ============== start events ===============
        $("#service-select").change(function () {
            $('.name-part').empty();
            $('.hospital-part').empty();
            $('.stuff-number-part').empty();
            if (this.value == text.hospital) {
                $('.hospital-part').append(content['hospital']);
                $('.stuff-number-part').append(content['stuff']);
            } else {
                $('.name-part').append(content['name']);

            }
        });
        // gender select
        $('#service-select').select2({
            selectOnClose: true,
            minimumResultsForSearch: Infinity

        });
        $('#bundle-select').select2({
            selectOnClose: true,
            minimumResultsForSearch: Infinity

        });
        $(document).ready(function () {
            $.getJSON("./img/flags/countries-ar.json", function (data) {

                var countries = Object.values(data);
                var countryShort = Object.keys(data);
                
                var html = '';
                for (var i = 0; i < countries.length; i++) {
                    var value = countries[i];
                    var countryImg = '<img src="./img/flags/' + countryShort[i] + '.svg" />'
                    html += '<option value="' + countryShort[i] + '">' + countryImg + value.name + '</option>';

                    // append countries to country-select
                    $('#country-select').append($('<option>', {
                        value: countryShort[i],
                        html: countryImg + value.name
                    }));

                    // append phone code to phone-select
                    $('#phone-code-select').append($('<option>', {
                        value: countryShort[i],
                        html: value.code
                    }))


                }

                // bind country select html tag
                function countryData(data) {
                    if (!data.id) { return data.text; }
                    var $countryResult = $(
                        '<div class="country-options" style="direction: ltr">' + '<span class="country-name" >' + data.text + '</span><span class="country-flag"><img src="./img/flags/' + data.id + '.svg"/> ' + '</span></div>'
                    );

                    return $countryResult;
                };

                // bind country phone select html tag
                function phoneCountryData(data) {
                    if (!data.id) { return data.text; }
                    var $phoneResult = $(
                        '<div class="country-options" style="direction: ltr">' + '<span class="country-name" >' + data.text + '</span><span class="country-flag"><img src="./img/flags/' + data.id + '.svg"/> ' + '</span></div>'
                    );

                    return $phoneResult;
                };

                $('#country-select').select2({
                    selectOnClose: true,
                    templateResult: countryData,
                    templateSelection: countryData
                });
                $('#phone-code-select').select2({
                    selectOnClose: true,
                    templateResult: phoneCountryData,
                    templateSelection: phoneCountryData
                });
            }).fail(function () {
                console.log("An error has occurred.");
            });
            content['name'] = $('.name-part').html();
            content['hospital'] = $('.hospital-part').html();
            content['stuff'] = $('.stuff-number-part').html();

            $('.hospital-part').empty();
            $('.stuff-number-part').empty();

        });
    // ============== end events ===============
