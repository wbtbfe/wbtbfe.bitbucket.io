$(function() {
  function isInViewport(element) {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 50 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
  $(window).scroll(function () {
    const button = $('.sticky-check').first()[0];
    const stickyElement =$('.sticky-down-button').first();
    const isSticky=stickyElement.hasClass('sticky-active')
    if (window.outerWidth < 1200) {
      if (isInViewport(button)) {
        if (isSticky) {
          setTimeout(function(){
            stickyElement
              .fadeOut(10)
              .removeClass("sticky-active")
              .fadeIn(400);
          },0)
        }
      } else {
        if (!isSticky) {
          setTimeout(function(){
            stickyElement
              .fadeOut(10)
              .addClass("sticky-active")
              .fadeIn(400);
          },0)
        }

      }
    }
  });
});