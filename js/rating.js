      // ============== start variables  ===============
      const text = {
        showRatingDetails: "إظهار تفاصيل التقييم",
        hideRatingDetails: "إخفاء تفاصيل التقييم",
      };
      var showChar = 300;
      var ellipsestext = "...";
      var moretext = "أقل";
      var lesstext = "المزيد";
      // ============== end variables ===============
      // ============== start events ===============

      $(".more").each(function () {
        var content = $(this).text();

        if (content.length > showChar) {
          var c = content.substr(0, showChar);
          var h = content.substr(showChar - 1, content.length - showChar);

          var html =
            c +
            '<span class="more-ellipses">' +
            ellipsestext +
            '</span>&nbsp;<span class="more-content"><span class="hide" style="display:none;">' +
            h +
            '</span>&nbsp;&nbsp;<a href="" class="more-link less">' +
            lesstext +
            "</a></span>";
          
          $(this).html(html);
        }
      });

      $(".more-link").click(function () {
        if ($(this).hasClass("less")) {
          $(this).removeClass("less");
          $(this).html(moretext);
        } else {
          $(this).addClass("less");
          $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
      });
      /* 1. Visualizing things on Hover - See next part for action on click */
      $(".stars li")
        .on("mouseover", function () {
          var onStar = parseInt($(this).data("value"), 10); // The star currently mouse on

          // Now highlight all the stars that's not after the current hovered star
          $(this)
            .parent()
            .children("li.star")
            .each(function (e) {
              if (e < onStar) {
                $(this).addClass("hover");
              } else {
                $(this).removeClass("hover");
              }
            });
        })
        .on("mouseout", function () {
          $(this)
            .parent()
            .children("li.star")
            .each(function (e) {
              $(this).removeClass("hover");
            });
        });

      /* 2. Action to perform on click */
      $(".stars li").on("click", function () {
        var onStar = parseInt($(this).data("value"), 10); // The star currently selected
        console.log("🚀 ~ file: doctor-about-clinic-new.html ~ line 3175 ~ onStar", onStar)
        var stars = $(this).parent().children("li.star");

        for (i = 0; i < stars.length; i++) {
          $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
          $(stars[i]).addClass("selected");
        }
      });

      $(".show-more").click(function () {
        let that = this;
        $(this)
          .siblings(".rating-detail")
          .toggle("fast", function () {
            $(this).removeClass("d-flex flex-column");
            if ($(this).is(":visible")) {
              $(this).addClass("d-flex flex-column");
              $(that).text(text.hideRatingDetails);
            } else {
              $(that).text(text.showRatingDetails);
            }
          });
      });
      // open rating modal
      $("#rating-column").click(function () {
        $("#rating-modal").show(0);
      });
      // open more detail
      $(".open-more-rating").click(function () {
        $("#rating-detail-modal").show(0);
      });
      // rating-detail-modal
      $(".modal-close").click(function () {
        $(this).parents(".modal").hide(0);
      });
      $(".modal")
        .click(function () {
          $(this).hide(0);
        })
        .children()
        .click(function (e) {
          return false;
        });
      $(".replay-option").click(function () {
        $(this).siblings(".replay-menu").toggle(0);
      });

      $(document).on("click", function (event) {
    
        var $trigger = $(".replay-option");
        
        if ($trigger.index(event.target) !== -1) {
          return;
        }
        $(".replay-menu").slideUp(0);
      });

      $(".edit-replay").click(function () {
        const replayText = $(this)
          .parents(".comment")
          .children(".replay-text")
          .text()
          .trim();
        $(this).parents(".comment").children(".replay-text").hide(0);
        $(this).parents(".comment").children(".edit-replay-body").show(0);
        $(this)
          .parents(".comment")
          .children(".edit-replay-body")
          .children(".edit-replay-text")
          .val(replayText);
      });
      $(".cancle-edit-replay").click(function () {
        $(this).parents(".edit-replay-body").hide(0);
        $(this).parents(".comment").children(".replay-text").show(0);
      });
