import "../css/bootstrap/bootstrap.min.css";
import "../css/tagsinput.css";
import "../fonts/stylesheet.css";
import "../css/master.css";

import "../js/vendor/modernizr-3.8.0.min";
import "../js/vendor/jquery-3.4.1.min";
import $ from 'jquery';
window.jQuery = $;
window.$ = $;