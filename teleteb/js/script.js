function uploadimg(element) {
  element.nextElementSibling.click();
}
function updateimg(element) {
  if (element.files && element.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      element.previousElementSibling.src = e.target.result;
    };
    reader.readAsDataURL(element.files[0]);
  }
}
function deleteimg(element) {
  element.previousElementSibling.previousElementSibling.src = "";
  element.previousElementSibling.value = "";
}
function opensearch() {
  $(".search-box").addClass("show");
  $(".search-box input").focus();
  if ($(".number-of-files") && $( window ).width()<768) {
    $(".number-of-files").hide();
  }
  if($( window ).width()<768)
  {

    $(".lead-para").hide();
  }
  $(".action-blk").hide();
}
function closesearch() {
  $(".search-box").removeClass("show");
  if ($(".number-of-files")) {
    $(".number-of-files").show();
  }
  $(".lead-para").show();
  $(".action-blk").show();
}
function openlogs() {
  $(".history").addClass("hide");
  $(".logs").removeClass("hide");
  $(".tabs").eq(0).addClass("active");
  $(".tabs").eq(1).removeClass("active");
}
function openhistory() {
  $(".logs").addClass("hide");
  $(".history").removeClass("hide");
  $(".tabs").eq(1).addClass("active");
  $(".tabs").eq(0).removeClass("active");
}

function toggle_collapse(elm){
  if($(elm).next().css('display')=="inline-block")
  {
    $(elm).find(".plus").attr('src','img/plus.svg');
    $(elm).next().css('display','none');
    $(elm).parent().next().addClass('d-none');
  }
  else{
    $(elm).find(".plus").attr('src','img/minus.svg');
    $(elm).next().css('display','inline-block');
    $(elm).parent().next().removeClass('d-none');
  }
  // console.log($(elm).next().css('display'));
  // $(elm).find(".plus").attr('src','img/minus.svg')
}